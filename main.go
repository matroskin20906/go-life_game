package main

import (
	"fmt"
	"time"
)

var directions = [][]int{
	{-1, -1},
	{0, -1},
	{-1, 0},
	{1, 1},
	{-1, 1},
	{1, -1},
	{0, 1},
	{1, 0},
}

func getNeighbourCount(field [][]int, row, col int) int {
	var neighbourCount int
	for _, direction := range directions {
		neighbourRow := row + direction[0]
		neighbourCol := col + direction[1]

		if neighbourRow < 0 || neighbourRow >= len(field) {
			continue
		}

		if neighbourCol < 0 || neighbourCol >= len(field[row]) {
			continue
		}

		neighbourCount += field[neighbourRow][neighbourCol]
	}
	return neighbourCount
}

func round(field [][]int) [][]int {
	result := make([][]int, len(field))
	for i := range field {
		result[i] = make([]int, len(field[i]))
	}

	for row := range field {
		for col := range field[row] {
			result[row][col] = field[row][col]
			neighbourCount := getNeighbourCount(field, row, col)
			if neighbourCount < 2 {
				result[row][col] = 0
			} else if neighbourCount > 3 {
				result[row][col] = 0
			} else if neighbourCount == 3 {
				result[row][col] = 1
			}
		}
	}

	return result
}

func printField(field [][]int) {
	fmt.Println("==================================")
	for row := range field {
		for col := range field[row] {
			if field[row][col] == 1 {
				fmt.Print("x")
			} else {
				fmt.Print(" ")
			}
		}
		fmt.Println()
	}
	fmt.Println("==================================")
}

func startGame() {
	field := make([][]int, 24)
	for row := range field {
		field[row] = make([]int, 80)
	}
	// here we need to inicialize some state
	// - - x x - -
	// - - x x - -
	// x x x - - -
	// - x - - - -
	field[10][14] = 1
	field[10][15] = 1
	field[11][14] = 1
	field[11][15] = 1
	field[12][12] = 1
	field[12][13] = 1
	field[12][14] = 1
	field[13][13] = 1
	timer := time.NewTicker(100 * time.Millisecond)
	for {
		<-timer.C
		printField(field)
		field = round(field)
	}
}

func main() {
	startGame()
}
